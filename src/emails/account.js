const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email,name) => {
    sgMail.send({
        to: email,
        from: 'nikola.jankovic@waltercode.com',
        subject: 'Thanks for joining in!',
        text: `Welcome to the app ${name}!`
    })
}

const sendCancelationMail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'nikola.jankovic@waltercode.com',
        subject: 'Stay with us!',
        text: `Actually you don't have to! Have a nice day ${name}`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancelationMail
}